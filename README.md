
# Android
## Instalation

You must use gradle to use Ipns Android. Add the repository and the library into your build.gradle.

```gradle
repositories {
    maven {
        url "http://nexus-indramov.rhcloud.com/nexus/content/repositories/releases/"
        }
}


dependencies {
    compile 'es.indra.ipns:ipns:0.3.2-SNAPSHOT'
    compile 'com.google.android.gms:play-services:6.5.87'
}
```


Update the AndroidManifest.xml to gran push permissions
```xml
<application>
...
<meta-data android:name="com.google.android.gms.version" android:value="@integer/google_play_services_version" />

    <receiver
    android:name="es.indra.ipns.gcm.GcmBroadcastReceiver"
    android:permission="com.google.android.c2dm.permission.SEND">

        <intent-filter>
            <action android:name="com.google.android.c2dm.intent.RECEIVE"/>
            <category android:name="es.indra.ipns"/>
        </intent-filter>
    </receiver>

<service android:name="es.indra.ipns.gcm.GcmIntentService"/>

<permission
android:name="es.indra.ipns.permission.C2D_MESSAGE"
android:protectionLevel="signature"/>

<uses-permission android:name="android.permission.INTERNET"/>
<uses-permission android:name="android.permission.WAKE_LOCK"/>
<uses-permission android:name="android.permission.VIBRATE"/>
<uses-permission android:name="com.google.android.c2dm.permission.RECEIVE"/>
<uses-permission android:name="es.indra.ipns.permission.C2D_MESSAGE"/>
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>

</application>
```

## Usage

Inicializate
```java
Ipns ipns = new Ipns.Builder(context)
        .endPoint("https://ipns-test.herokuapp.com")
        .senderId("148119910980")
        .appId("ipnsExample")
        .onIpnsListener(this)
        .build();
```

Register
```java

ipns.register(businessId);
```
Unregister
```java
ipns.unregister();
```
# IOS
>Coming soon
# Phonegap
## Installation
Install it as usual phongap plugin, copy IpnsPlugin in your project:
```sh
$ cordova plugin add IpnsPlugin
```
##Usage
The function you have to use are declared in IpnsPlugin.

You can include two function like a callback, the first one is SuccesCallback, this callbacks you will recevie a IpnsUser in Json format. And the other one is ErrorCallback, you just will receive error message.

Example:
```javascript
    ipnsPlugin.register("http://ipns.herokuapp.com","businessId","appID",
                        function(ipnsUser){
                            console.log("PushRegistion, "+ ipnsUser.id);
                        },function(error){
                            console.log("No Push " + error);
                        });

    ipnsPlugin.unregister("http://ipns.herokuapp.com”,userId,
                        function(messager){
                            console.log(“Deleted, "+ message);
                        },function(error){
                            console.log("No Push " + error);
                        });
```
# Api Rest
>[API DOC](http://docs.ipns.apiary.io/)


